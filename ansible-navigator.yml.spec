---
# these are additional configurations to place in your project for use with ansible-navigator
ansible-navigator:

  execution-environment:
  # naturally this can be a source of your choice
  # this is an image I built locally from the content in meta via:
  # ansible-builder build --tag localhost/redhatautomation-terraform-demo-minimal
    image: localhost/redhatautomation-terraform-demo-minimal 
  # since I have a local build already
    pull-policy: never 
  # just _A_ method of giving the execution environment access to the necessary AWS credentials
  # not necessary for Controller (Tower) with those credentials configured
    volume-mounts:
    - src: "/home/user/.aws"
      dest: "/home/runner/.aws"

  # just a personal preference
  playbook-artifact:
    enable: True
    replay: /tmp/terraform_demo.json
    save-as: /tmp/terraform_demo.json